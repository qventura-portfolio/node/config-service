import { Constants as ServiceConstants } from '../config';

const ROOT_PATH = `/${ServiceConstants.SERVICE_NAME_PATH}`;

export const Constants = {
  ROOT_PATH,
}
