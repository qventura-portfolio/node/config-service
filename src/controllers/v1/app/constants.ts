import { Constants as ParentConsants } from '..';

const ROOT_PATH = `${ParentConsants.ROOT_PATH}/app`;

export const Constants = {
  ROOT_PATH,
}
