import {
  Instrumented,
} from '@qventura/base-service/dist/decorators';

import {
  BaseController,
  HttpMethods,
  Route,
  Serializer
} from '@qventura/base-service/dist/controllers';

import { App } from '../../../services';
import { Constants } from '.';

const ROOT_PATH = Constants.ROOT_PATH;

export class Controller extends BaseController {
  @Instrumented()
  static async read(ctx: any, next: Function) : Promise<void> {
    const osType = ctx.request.query.os_type;
    const version = ctx.request.query.version;

    const data = await new App.Service(ctx).getOne({ osType, version });

    ctx.response.status = 200;
    ctx.response.body = { app: Serializer.serialize(data, App.Serializer) };

    await next();
  }

  static getRoutes() : Route[] {
    return [
      {
        method: HttpMethods.GET,
        path: ROOT_PATH,
        resolver: Controller.read
      }
    ]
  }
}
