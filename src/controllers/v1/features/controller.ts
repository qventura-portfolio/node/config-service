import {
  Authenticated,
  Instrumented,
  ValidateRequiredPermissions,
} from '@qventura/base-service/dist/decorators';

import {
  BaseController,
  HttpMethods,
  Route,
  Serializer
} from '@qventura/base-service/dist/controllers';

import { Features } from '../../../services';
import { Constants } from '.';

const ROOT_PATH = Constants.ROOT_PATH;
const PATH_WITH_ID = `${ROOT_PATH}/:id`;

export class Controller extends BaseController {
  @Instrumented()
  @Authenticated()
  // @ValidateRequiredPermissions({ entityType: 'Feature' , action: 'create' })
  static async create(ctx: any, next: Function) : Promise<void> {
    const data = await new Features.Service(ctx).create(ctx.request.body.features);

    ctx.response.status = 201;
    ctx.response.body = { features: Serializer.serialize(data, Features.Serializer) };

    await next();
  }

  @Instrumented()
  @Authenticated()
  // @ValidateRequiredPermissions({ entityType: 'Feature' , action: 'delete' })
  static async delete(ctx: any, next: Function) : Promise<void> {
    const id = ctx.params.id;
    const data = await new Features.Service(ctx).delete({ id });

    ctx.response.status = 204;

    await next();
  }

  @Instrumented()
  @Authenticated()
  // @ValidateRequiredPermissions({ entityType: 'Feature' , action: 'read' })
  static async readAll(ctx: any, next: Function) : Promise<void> {
    const data = await new Features.Service(ctx).getAll();

    ctx.response.status = 200;
    ctx.response.body = { features: Serializer.serialize(data, Features.Serializer) };

    await next();
  }

  @Instrumented()
  @Authenticated()
  // @ValidateRequiredPermissions({ entityType: 'Feature' , action: 'update' })
  static async update(ctx: any, next: Function) : Promise<void> {
    const id = ctx.params.id;
    const data = await new Features.Service(ctx).update({ id }, ctx.request.body.features);

    ctx.response.status = 200;
    ctx.response.body = { features: Serializer.serialize(data, Features.Serializer) };

    await next();
  }

  static getRoutes() : Route[] {
    return [
      {
        method: HttpMethods.POST,
        path: ROOT_PATH,
        resolver: Controller.create
      },
      {
        method: HttpMethods.DELETE,
        path: ROOT_PATH,
        resolver: Controller.delete
      },
      {
        method: HttpMethods.GET,
        path: ROOT_PATH,
        resolver: Controller.readAll
      },
      {
        method: HttpMethods.PUT,
        path: PATH_WITH_ID,
        resolver: Controller.update
      }
    ]
  }
}
