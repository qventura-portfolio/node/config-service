import { Constants as ParentConsants } from '..';

const ROOT_PATH = `${ParentConsants.ROOT_PATH}/v1`;

export const Constants = {
  ROOT_PATH,
}
