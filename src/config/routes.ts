import {
  HttpMethods,
  Route,
} from '@qventura/base-service';

import {
  Constants
} from '.';

import {
  V1,
} from '../controllers';

export const Routes: Route[] = [
  ...V1.App.Controller.getRoutes(),
  ...V1.Features.Controller.getRoutes(),
];

export default Routes;
