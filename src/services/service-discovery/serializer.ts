export class Serializer {
  static serialize(data) {
    return {
      name: data.name,
      url: data.url
    }
  }
}
