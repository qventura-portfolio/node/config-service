import { Instrumented } from '@qventura/base-service/dist/decorators';
import { VersionNotSupportedError } from '@qventura/base-service/dist/errors';
import { ConfigManager } from '@qventura/base-service/dist/lib';

import { Features } from '..';

import { Model } from '.';

export class Service {
  constructor(private ctx) {}

  @Instrumented()
  public async getAll() : Promise<any> {

    return await ConfigManager.getInstance().getServiceUrls();
  }
}
