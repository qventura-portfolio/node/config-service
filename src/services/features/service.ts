import { Instrumented } from '@qventura/base-service/dist/decorators';
import { CQRSService } from '@qventura/base-service/dist/services';

import { Command, Query } from '.';

const ENABLE_FEATURE_EVENT_ENTITY_TYPE = 'EnabledFeature';
const DISABLE_FEATURE_EVENT_ENTITY_TYPE = 'DisableFeature';

export class Service extends CQRSService {
  constructor(private ctx) {
    super(ctx.state, Command, Query)
  }

  @Instrumented()
  public create(args) {
    return super.create({ ...args });
  }

  @Instrumented()
  public delete({ id }) {
    return super.delete({ _id: id });
  }

  @Instrumented()
  public getAll() {
    return super.get({});
  }

  @Instrumented()
  public update({ id }, args) {
    if(args.enabled) {
      return super.update({ _id: id }, args, ENABLE_FEATURE_EVENT_ENTITY_TYPE);
    }

    return super.update({ _id: id }, args, DISABLE_FEATURE_EVENT_ENTITY_TYPE);
  }
}
