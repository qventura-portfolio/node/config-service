export class Serializer {
  static serialize(data) {
    return {
      id: data._id,
      key: data.key,
      enabled: data.enabled
    }
  }
}
