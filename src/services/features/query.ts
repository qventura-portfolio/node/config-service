import * as mongoose from 'mongoose';

const CriterasSchema = new mongoose.Schema({
  type: {
    type: String,
    required: true
  },
  id: {
    type: String,
    required: true
  }
}, {
  timestamps: true
})

const FeatureSchema = new mongoose.Schema({
  key: {
    type: String,
    required: true
  },
  enabled: {
    type: Boolean,
    required: false
  }
},{
  timestamps: true
});

FeatureSchema.index({ key: 1 })
FeatureSchema.index({ key: 1, enabled: 1 })

export const Query = mongoose.model('Feature', FeatureSchema, 'features');
