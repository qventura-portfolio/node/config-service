import * as mongoose from 'mongoose';
import { CommandSchema } from '@qventura/base-service/dist/services';

const schema = mongoose.Schema(...CommandSchema)

export const Command = mongoose.model('FeaturesCommand', schema, 'events');
