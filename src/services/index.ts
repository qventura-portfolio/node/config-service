import * as App from './app';
import * as Features from './features';
import * as ServiceDiscovery from './service-discovery';

export {
  App,
  Features,
  ServiceDiscovery,
}
