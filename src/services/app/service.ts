import { Instrumented } from '@qventura/base-service/dist/decorators';
import { VersionNotSupportedError } from '@qventura/base-service/dist/errors';
import { ConfigManager } from '@qventura/base-service/dist/lib';

import { Features, ServiceDiscovery } from '..';

import { Model } from '.';

export class Service {
  constructor(private ctx) {}

  @Instrumented()
  public async getOne({ version, osType }) {
    const sanitizedAppType = (osType || 'ios');

    const minimumSupportedVersionPromise = ConfigManager.getInstance().getGlobalVariable(`MinimumSupportedVersion/${sanitizedAppType}`);
    const featuresPromise = new Features.Service(this.ctx).getAll();
    const serviceDiscoveryPromise = new ServiceDiscovery.Service(this.ctx).getAll();

    const [minimumSupportedVersion, features, services] = await Promise.all([
      minimumSupportedVersionPromise,
      featuresPromise,
      serviceDiscoveryPromise
    ])

    return new Model({
      features,
      osType: sanitizedAppType,
      minimumSupportedVersion,
      services,
      version
    });
  }
}
