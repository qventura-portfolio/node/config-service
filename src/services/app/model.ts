export interface ModelSchema {
  features: [any],
  minimumSupportedVersion: string;
  osType: string;
  version: string;
  services: [any];
}

export class Model {
  public features;
  public minimumSupportedVersion;
  public osType;
  public version;
  public services;

  constructor(args : ModelSchema) {
    this.features = args.features;
    this.minimumSupportedVersion = args.minimumSupportedVersion;
    this.osType = args.osType;
    this.version = args.version;
    this.services = args.services;
  }

  public requireUpdate() : boolean {
    const sanitizedVersion = parseInt(this.version);
    const sanitizedMinimumSupportedVersion = parseInt(this.minimumSupportedVersion);

    return sanitizedMinimumSupportedVersion > sanitizedVersion;
  }
}
