import { Serializer as BaseSerializer } from '@qventura/base-service/dist/controllers';

import { Features, ServiceDiscovery } from '..';

export class Serializer {
  public static serialize(data) {
    return {
      features: BaseSerializer.serialize(data.features, Features.Serializer),
      services: BaseSerializer.serialize(data.services, ServiceDiscovery.Serializer),
      minimumSupportedVersion: data.minimumSupportedVersion,
      requireUpdate: data.requireUpdate()
    };
  }
}
