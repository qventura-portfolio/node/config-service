import { ModuleTypes } from '@qventura/base-service/dist/config';
import { initNewrelic } from '@qventura/base-service/dist/lib';
import { startHttpServer } from '@qventura/base-service';

import * as mongoose from 'mongoose';

import { Constants, Routes } from './config';

try {
  startHttpServer({
    serviceName: Constants.SERVICE_NAME,
    servicePath: Constants.SERVICE_NAME_PATH,
    environment: process.env.NODE_ENV,
    port: process.env.PORT,
    modules: [ {
        type: ModuleTypes.Mongoose,
        package: mongoose,
      }
    ],
    routes: Routes,
  })
}
catch(error) {
  console.error(error);
  process.exit(1);
};
