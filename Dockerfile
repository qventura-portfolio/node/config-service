FROM keymetrics/pm2:8-alpine

RUN apk add --no-cache make gcc g++ python

ARG NPM_TOKEN
RUN npm config set //registry.npmjs.org/:_authToken $NPM_TOKEN
RUN npm config set loglevel warn
RUN npm install -g typescript

RUN mkdir -p /var/app
WORKDIR /var/app

COPY package.json /var/app/
COPY package-lock.json /var/app/

RUN npm install --production

COPY . .

RUN npm run build

EXPOSE 5000

CMD npm start
